# jwtcli

A Rust CLI for generating JWKS/JWTs

This is mostly a wrapper around [biscuit](https://github.com/lawliet89/biscuit).


## Usage

Detailed usage information is available under the `-h` flag.

There are two primary subcommands: `jwk` and `jwt`. The former can be used to
generate JWK and JWKS files. For example:

This will generate a new JWK (piped to a file named `my-jwk`) using the default
HS256 algorithm.
```
jwtcli jwk > my-jwk
```

Then, to generate a jwt from that key, setting some claim information:
```
jwtcli jwt --jwks my-jwk -c aud=tpfs -c iss=mario > my-jwt
```

You can also generate a JWT from a raw string:
```
jwtcli jwt --secret "secret string" -c aud=tpfs -c iss=mario > my-jwt
```
