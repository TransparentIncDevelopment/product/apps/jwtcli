use std::time::SystemTimeError;

impl From<std::time::SystemTimeError> for super::Error {
    fn from(s: SystemTimeError) -> Self {
        super::Error::SystemTimeError(s.to_string())
    }
}
