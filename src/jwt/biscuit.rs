use biscuit::{
    jws::{Header, Secret},
    ClaimsSet, Empty, JWT,
};
use serde_json::Value;
use std::collections::HashMap;

use crate::jwt::error::Result;

pub fn make_jwt<S: Into<String>>(
    claims: Vec<(S, Value)>,
    secret: Vec<u8>,
) -> Result<biscuit::jws::Compact<ClaimsSet<HashMap<String, String>>, Empty>> {
    let header = Header::<Empty>::default();
    let claims = kvp_to_claim(claims)?;
    let jwt = JWT::new_decoded(header, claims);
    let jwt = jwt.into_encoded(&Secret::Bytes(secret))?;
    Ok(jwt)
}

fn kvp_to_claim<S: Into<String>>(
    claims: Vec<(S, Value)>,
) -> Result<ClaimsSet<HashMap<String, String>>> {
    let claims: HashMap<String, Value> = claims.into_iter().map(|(a, b)| (a.into(), b)).collect();
    // We serialize back and forth to dramatically shorten the code. The biscuit API
    // is well typed, which is nice, but makes constructing the claims from arbitrary
    // input quite verbose - so in this case we just pass it along. The "private" type
    // in the `ClaimSet` being a map allows any custom claims to be passed in.
    let claims_str = serde_json::to_string(&claims)?;
    let claims: ClaimsSet<HashMap<String, String>> = serde_json::from_str(&claims_str)?;
    Ok(claims)
}

#[cfg(test)]
mod test {
    use super::*;
    use biscuit::jwa::SignatureAlgorithm;

    #[test]
    fn make_jwt_validates() {
        let claims = vec![("iss", "2Pac".into()), ("aud", "Biggie".into())];
        let secret = "To Live and Die in LA";
        let jwt = make_jwt(claims.clone(), secret.as_bytes().to_vec()).unwrap();
        // The act of decoding is also verification
        let jwt = jwt
            .into_decoded(
                &Secret::Bytes(secret.as_bytes().to_vec()),
                SignatureAlgorithm::HS256,
            )
            .unwrap();
        assert_eq!(*jwt.payload().unwrap(), kvp_to_claim(claims).unwrap());
    }
}
