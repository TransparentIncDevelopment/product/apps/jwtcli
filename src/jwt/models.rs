use std::path::PathBuf;

#[derive(Debug, Clone)]
pub enum ClaimValue {
    Integer(u64),
    String(String),
}

impl From<ClaimValue> for serde_json::Value {
    fn from(j: ClaimValue) -> Self {
        match j {
            ClaimValue::Integer(i) => serde_json::Value::Number(i.into()),
            ClaimValue::String(s) => serde_json::Value::String(s),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Claim(String, ClaimValue);
impl Claim {
    pub fn new(key: String, val: ClaimValue) -> Self {
        Self(key, val)
    }

    pub fn key(&self) -> String {
        self.0.clone()
    }

    pub fn value(&self) -> ClaimValue {
        self.1.clone()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct JwtSecret(String);
impl JwtSecret {
    pub fn new(secret: String) -> Self {
        Self(secret)
    }

    pub fn value(&self) -> String {
        self.0.clone()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct JwtToken(String);
impl JwtToken {
    pub fn new(token: String) -> Self {
        Self(token)
    }

    pub fn value(&self) -> String {
        self.0.clone()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct JwksPath(PathBuf);
impl JwksPath {
    pub fn new(path: PathBuf) -> Self {
        Self(path)
    }

    pub fn value(&self) -> PathBuf {
        self.0.clone()
    }
}
