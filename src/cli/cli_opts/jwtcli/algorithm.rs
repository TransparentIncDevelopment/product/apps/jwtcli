use anyhow::Error;
use std::convert::{TryFrom, TryInto};

pub struct CliAlgorithmInput(pub String);
impl From<String> for CliAlgorithmInput {
    fn from(s: String) -> Self {
        CliAlgorithmInput(s)
    }
}
impl TryFrom<CliAlgorithmInput> for jwtcli::jwk::models::Algorithm {
    type Error = Error;

    fn try_from(value: CliAlgorithmInput) -> Result<Self, Self::Error> {
        match value.0.as_str() {
            "HS256" => Ok(jwtcli::jwk::models::Algorithm::HS256),
            "HS384" => Ok(jwtcli::jwk::models::Algorithm::HS384),
            "HS512" => Ok(jwtcli::jwk::models::Algorithm::HS512),
            _ => Err(anyhow!("Unsupported algorithm")),
        }
    }
}
pub fn try_algorithm_cli_input_to_algorithm(
    cli_input: String,
) -> Result<jwtcli::jwk::models::Algorithm, Error> {
    CliAlgorithmInput::from(cli_input).try_into()
}
