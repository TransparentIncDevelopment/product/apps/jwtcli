use anyhow::Error;
use std::path::PathBuf;
use structopt::{clap::ArgGroup, StructOpt};

pub mod jwtcli;

#[derive(Debug, StructOpt)]
#[structopt(name = "jwtcli", about = "For generating JWKS and JWTs")]
pub struct Opt {
    #[structopt(subcommand)]
    pub cmd: Cmd,
}

#[derive(StructOpt, Debug)]
pub enum Cmd {
    /// Generate a JWK and write it to stdout
    Jwk {
        /// The algorithm to use
        #[structopt(short, long, default_value = "HS256")]
        algorithm: String,
        /// If provided, set the `kid` field to this value
        #[structopt(short, long)]
        key_id: Option<String>,
        /// If set, models will be in JWKS format, including only one JWK (the one being generated)
        #[structopt(long)]
        as_jwks: bool,
    },

    /// Generate a JWT from an existing JWKS or from an alternate source and write it to stdout.
    /// Note that by default an expiry time of one year is used unless `exp` is set explicitly using
    /// the `--claims` argument.
    #[structopt(group = ArgGroup::with_name("secretsource").required(true))]
    Jwt {
        /// Set claims. May be specified multiple times. EX: `-c iss=mycompany -c aud=tpfs`
        #[structopt(short, long, parse(try_from_str = parse_key_val), number_of_values = 1)]
        claims: Vec<(String, String)>,
        /// Secret in string form (exclusive with --jwks)
        #[structopt(short, long, group = "secretsource")]
        secret: Option<String>,
        /// Path to a JWK(S) file to use for generating the JWT. If the file is a JWKS, the first
        /// key is used. (exclusive with --secret)
        #[structopt(short, long, group = "secretsource")]
        jwks: Option<PathBuf>,
    },
}

/// Parse a single key-value pair
fn parse_key_val<T, U>(s: &str) -> Result<(T, U), Error>
where
    T: std::str::FromStr,
    T::Err: std::error::Error + Send + Sync + 'static,
    U: std::str::FromStr,
    U::Err: std::error::Error + Send + Sync + 'static,
{
    let pos = s
        .find('=')
        .ok_or_else(|| anyhow!("invalid KEY=value: no `=` found in `{}`", s))?;
    Ok((s[..pos].parse()?, s[pos + 1..].parse()?))
}
