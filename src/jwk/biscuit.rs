use biscuit::{
    jwa::{Algorithm, SignatureAlgorithm},
    jwk::{CommonParameters, JWKSet, PublicKeyUse, JWK},
    Empty,
};
use rand::RngCore;
use std::path::PathBuf;

use crate::jwk::error::{Error, Result};

/// Exists so serde can work for us to figure out what format was used during deserialization
#[allow(clippy::large_enum_variant)]
#[derive(Serialize, Deserialize)]
#[serde(untagged)]
enum JwkOrJwks<T> {
    Jwk(JWK<T>),
    Jwks(JWKSet<T>),
}

/// Generate a JWK for signing using the octet shared secret method.
pub fn gen_octet_jwk(alg: SignatureAlgorithm, key_id: Option<String>) -> JWK<Empty> {
    let mut rng = rand::thread_rng();
    // The 256 here doesn't have anything to do with HS256, it's just a reasonable length for a
    // random secret.
    let mut rando_bytes = vec![0; 256];
    rng.fill_bytes(&mut rando_bytes);
    let mut jwk = JWK::new_octet_key(&rando_bytes, Empty::default());
    let cp = CommonParameters {
        algorithm: Some(Algorithm::Signature(alg)),
        public_key_use: Some(PublicKeyUse::Signature),
        key_id,
        ..Default::default()
    };
    jwk.common = cp;
    jwk
}

/// Generate a JWKS for signing using the octet shared secret method.
pub fn gen_octet_jwks(alg: SignatureAlgorithm, key_id: Option<String>) -> JWKSet<Empty> {
    let jwk = gen_octet_jwk(alg, key_id);
    JWKSet { keys: vec![jwk] }
}

/// Given a path to a file that may be either a JWK or a JWKS, extract the octet key from the JWK
/// (or the first JWK in the JWKS set).
pub fn get_jwk_key(jwks: PathBuf) -> Result<Vec<u8>, Error> {
    let content = std::fs::read_to_string(jwks)?;
    let jwkmaybe: JwkOrJwks<Empty> = serde_json::from_str(&content)?;
    let jwk = match &jwkmaybe {
        JwkOrJwks::Jwk(j) => j,
        JwkOrJwks::Jwks(j) => j.keys.get(0).ok_or(Error::NoJWKInJWKSFile)?,
    };
    Ok(jwk.octet_key().map_err(Error::from)?.to_vec())
}
