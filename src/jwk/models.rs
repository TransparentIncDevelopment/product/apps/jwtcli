pub mod biscuit;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Jwk(pub String);
impl Jwk {
    pub fn new(jwk: String) -> Self {
        Self(jwk)
    }

    pub fn value(&self) -> String {
        self.0.clone()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Jwks(Vec<Jwk>);
impl Jwks {
    pub fn new(jwks: Vec<Jwk>) -> Self {
        Self(jwks)
    }

    pub fn value(&self) -> Vec<Jwk> {
        self.0.clone()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Algorithm {
    HS256,
    HS384,
    HS512,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct KeyId(String);
impl KeyId {
    pub fn new(key_id: String) -> Self {
        Self(key_id)
    }

    pub fn value(&self) -> String {
        self.0.clone()
    }
}
