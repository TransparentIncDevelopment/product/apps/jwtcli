use std::convert::TryFrom;

use biscuit::{
    jwa::SignatureAlgorithm,
    jwk::{JWKSet, JWK},
    Empty,
};

use crate::jwk::{
    error::Error,
    models::{Algorithm, Jwk, Jwks},
};

impl TryFrom<JWK<Empty>> for Jwk {
    type Error = Error;

    fn try_from(j: JWK<Empty>) -> Result<Self, Self::Error> {
        Ok(Jwk::new(serde_json::to_string(&j)?))
    }
}

impl TryFrom<JWKSet<Empty>> for Jwks {
    type Error = Error;

    fn try_from(j: JWKSet<Empty>) -> Result<Self, Self::Error> {
        let mut jwks = vec![];
        for key in j.keys {
            jwks.push(Jwk::try_from(key)?)
        }
        Ok(Jwks(jwks))
    }
}

impl From<Algorithm> for SignatureAlgorithm {
    fn from(s: Algorithm) -> Self {
        match s {
            Algorithm::HS256 => SignatureAlgorithm::HS256,
            Algorithm::HS384 => SignatureAlgorithm::HS384,
            Algorithm::HS512 => SignatureAlgorithm::HS512,
        }
    }
}
