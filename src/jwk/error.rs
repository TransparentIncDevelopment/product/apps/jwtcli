use thiserror::Error;

pub mod biscuit;
pub mod io;
pub mod serde_json;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Clone, Debug, Error, Eq, PartialEq)]
pub enum Error {
    #[error("{0}")]
    SerdeJsonError(String),
    #[error("{0}")]
    BiscuitError(String),
    #[error("No JWK present in JWKS file.")]
    NoJWKInJWKSFile,
    #[error("{0}")]
    IoError(String),
}
